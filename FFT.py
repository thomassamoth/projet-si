'''fichier provenant de http://samcarcagno.altervista.org/blog/basic-sound-processing-python/?doing_wp_cron=1557400803.9384551048278808593750'''

from pylab import*
from scipy.io import wavfile
import matplotlib.pyplot as plt

sampFreq, snd = wavfile.read('440_sine.wav')
print('Sample frequency = ', sampFreq)
print('Snd type = ', snd.dtype)
snd = snd / (2**15)
print('Shape snd = ',snd.shape)
print(5292 / sampFreq)
s1 = snd[:,0]

#Plotting the tone

timeArray = arange(0, 5292, 1)
timeArray = timeArray / sampFreq
timeArray = timeArray * 1000  #scale to milliseconds

plt.plot(timeArray, s1, color='k')
plt.show()
ylabel('Amplitude')
xlabel('Time (ms)')

n = len(s1)
p = fft(s1) # take the fourier transform
nUniquePts = int(ceil((n+1)/2.0))
p = p[0:nUniquePts]
p = abs(p)
p = p / float(n)
print(p)
# scale by the number of points so that
# the magnitude does not depend on the length
# of the signal or on its sampling frequency p = p**2
# square it to get the power
# multiply by two (see technical document for details)
# odd nfft excludes Nyquist point if n % 2 > 0:
# we've got odd number of points fft p[1:len(p)] = p[1:len(p)] * 2 else: p[1:len(p) -1] = p[1:len(p) - 1] * 2
# we've got even number of points fft freqArray = arange(0, nUniquePts, 1.0) * (sampFreq / n); plot(freqArray/1000, 10*log10(p), color='k') xlabel('Frequency (kHz)') ylabel('Power (dB)')

p = p / float(n) # scale by the number of points so that
                 # the magnitude does not depend on the length
                 # of the signal or on its sampling frequency
p = p**2  # square it to get the power

# multiply by two (see technical document for details)
# odd nfft excludes Nyquist point
if n % 2 > 0: # we've got odd number of points fft
    p[1:len(p)] = p[1:len(p)] * 2
else:
    p[1:len(p) -1] = p[1:len(p) - 1] * 2 # we've got even number of points fft

freqArray = arange(0, nUniquePts, 1.0) * (sampFreq / n);
lt.plot(freqArray/1000, 10*log10(p), color='k')

xlabel('Frequency (kHz)')
ylabel('Power (dB)')
rms_val = sqrt(mean(s1**2))
print('rms_val=', rms_val)