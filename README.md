# :wrench: PROJET SCIENCE DE L'INGÉNIEUR 

Projet réalisé dans le cadre du PISI de Terminale S SI  

:warning: _Projet transféré sur_ [**GitHub**](http://github.com/thomassamoth/violin-tuner)
### Accordeur de violon    

 1. Enregistrement du son et détéction de la fréquence jouée
 2. Rotation du moteur en fonction de la fréquence reçue
 3. User Interface grâce à des LED sur le boitier

 **Code final utilisé pour la présentation et la démo :**
https://gitlab.com/thomassamoth/projet-si/-/snippets/1931397

