#CHOIX DES NOTES
note_choisie = input('Choisir la note : ') #On demande à l'utilisteur de choisir la note qu'il veut accorder
if note_choisie == 'sol':
    frequence_cible = 196
elif note_choisie == 're':
    frequence_cible = 294
elif note_choisie == 'la':
    frequence_cible = 440
elif note_choisie == 'mi':
    frequence_cible = 660
else:
    print("Veuillez réessayer"):

#ENREGISTREMENT
import pyaudio
import wave
import datetime

current_date = datetime.datetime.now()
current_date = str(current_date)


FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
CHUNK = 1024
RECORD_SECONDS = 5
#WAVE_OUTPUT_FILENAME = "son.wav"
WAVE_OUTPUT_FILENAME = "note_choisie",  +current_date+ ".wav"


audio = pyaudio.PyAudio()

                                        # Debut enregistrement
stream = audio.open(format=FORMAT, channels=CHANNELS,
                rate=RATE, input=True,
                frames_per_buffer=CHUNK)
print ("Enregistrement en cours...")

frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)

print ("Fin d'enregistrement")


                                        # Fin enregistrement
stream.stop_stream()
stream.close()
audio.terminate()

waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
waveFile.setnchannels(CHANNELS)
waveFile.setsampwidth(audio.get_sample_size(FORMAT))
waveFile.setframerate(RATE)
waveFile.writeframes(b''.join(frames))
waveFile.close()



#PARTIE FFT
#import bibliotheque
import math
import numpy as np
from matplotlib.pyplot import *
import scipy.io.wavfile as wave
from numpy.fft import fft
import os
import time
#Fichier que l'on veut joindre
FILE = os.path.join(r'C:\Users\Thomas-scolaire\440.wav')

#Lecture du fichier son
RATE, DATA = wave.read(FILE)
TAB = np.array(DATA)
#np.set_printoptions(threshold=sys.maxsize)
#print('tab=',TAB)

#time.sleep(10)
n = DATA.size
DUREE = 1.0*n/RATE

#Affichage du spectre du son
te = 1.0/RATE
t = np.zeros(n)
for k in range(n):
    t[k] = te*k
figure(figsize=(12, 4))
plot(t, DATA)       
xlabel("t (s)")
ylabel("amplitude") 
axis([0, 0.3, DATA.min(), DATA.max()])
title('spectre')
grid(100)
#plot()

#Calul de la transformée de Fourier
def tracerFFT(DATA, RATE, debut, DUREE):
    start = int(debut*RATE)
    stop = int((debut+DUREE)*RATE)
    spectre = np.absolute(fft(DATA[start:stop]))

    spectre = spectre/spectre.max()
    n = spectre.size
  
    freq = np.zeros(n)
    for k in range(n):
        freq[k] = 1.0/n*RATE*k
        if spectre[k]==1 and freq[k]<1500:
            frequence_jouee_interne = freq[k]
    return [frequence_jouee_interne]
 # freq, couleur fond, spectre, couleur lignes 
#vlines(freq, 0, spectre, 'r')
 #   xlabel('f (Hz)')
  #  ylabel('A')
   # title('Transformée de Fourier')
    #axis([0, 0.5*RATE, 0, 1])
    #grid()'''

#Affichage de la FFT
figure(figsize=(12, 4))          #règle la taille des fenetres
FrequenceJouee=tracerFFT(DATA, RATE, 0.0, 0.5)  #DATA,RATE,debut,DUREE
axis([0, 1000, 0, 1])            #axes xmin,xmax,ymin,ymax
#show()                          #affiche grahique
print('frequence=', FrequenceJouee)
