#import bibliotheque
import math
import numpy as np
from matplotlib.pyplot import *
import scipy.io.wavfile as wave
from numpy.fft import fft
import os

#Fichier que l'on veut joindre
file = os.path.join(r'C:\Users\Thomas-scolaire\Desktop\Fichier_python_projet_SI\la-440-mono.wav')

#Lecture du fichier son
rate,data = wave.read(file)
n = data.size
duree = 1.0*n/rate

#Affichage du spectre du son
te = 1.0/rate
t = np.zeros(n)
for k in range(n):
    t[k] = te*k
figure(figsize=(12,4))
plot(t,data)        
xlabel("t (s)")
ylabel("amplitude") 
axis([0,0.1,data.min(),data.max()])
grid()

#Calul de la transformée de Fourier
def tracerFFT(data,rate,debut,duree):
    start = int(debut*rate)
    stop = int((debut+duree)*rate)
    spectre = np.absolute(fft(data[start:stop]))
    spectre = spectre/spectre.max()
    n = spectre.size
    freq = np.zeros(n)
    for k in range(n):
        freq[k] = 1.0/n*rate*k
    vlines(freq,[0],spectre,'r')
    xlabel('f (Hz)')
    ylabel('A')
    title('Transformée de Fourier')
    axis([0,0.5*rate,0,1])
    grid()
    #Calcul de la fréquence jouée
    max_y = np.max(freq/100)  # Find the maximum y value  # Find the x value corresponding to the maximum y value
    print(max_y,'Hz')

#Affichage de la FFT
figure(figsize=(12,4))          #règle la taille des fenetres
tracerFFT(data,rate,0.0,0.5)    #data,rate,debut,duree
axis([0,1000,0,1])              #axes xmin,xmax,ymin,ymax
show()                          #affiche grahique
