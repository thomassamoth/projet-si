'''Accordeur de violon'''

#import des librairies
import time #librairie qui permet de mettre un notion de temps
import pyaudio #librairie qui permet de gerer des fichiers sonores
import wave #permet de gerer des fichiers en .wav
import matplotlib.pyplot as plt


#Demander la note que l'on veux accorder
note_choisie = input('Choisir la note : ') #On demande à l'utilisteur de choisir la note qu'il veut accorder

if note_choisie == 'sol':
    frequence_cible = 196

elif note_choisie == 're':
    frequence_cible = 294

elif note_choisie == 'la':
    frequence_cible = 440

elif note_choisie == 'mi':
    frequence_cible = 660

print('Note choisie= ', note_choisie);
print ('Fréquence cible = ', frequence_cible);


#pause dans le prgramme
pause = 2
time.sleep(pause)
print('fin de la pause de ', pause, 'secondes')


#allume la led en jaune et l'eteint quand la pause et finie
#[MAIWENN]

#enregistrement du son
CHUNK = 1024 
FORMAT = pyaudio.paInt16 #paInt8
CHANNELS = 2 
RATE = 88200 #sample rate
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = (output + ".wav")

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK) #buffer

print("* recording")

frames = []


#transformation de Fourier

from scipy.fftpack import fft
from scipy.io import wavfile # get the api
fs, data = wavfile.read('output.wav') # charge le fichier
a = data.T[0] # this is a two channel soundtrack, I get the first track
b=[(ele/2**8.)*2-1 for ele in a] # this is 8-bit track, b is now normalized on [-1,1)
c = fft(b) #calcule la transformée de Fourier (liste de nombres complexes)
d = len(c)/2  # pour avoir seulement les nombre positifs
plt.plot(abs(c[:(d-1)]),'r') 
plt.show()
